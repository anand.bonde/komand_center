from django.shortcuts import render
from django.http import HttpResponse

from .models import Posts, Assignments, KidsAssignments


def index(request):
    posts = Posts.objects.all()[:10]
    assignments = Assignments.objects.order_by('-created_at')[:10]
    kids_assignments = KidsAssignments.objects.all()[:10]
    context = {
    	'posts': posts,
	'assignments': assignments,
	'kids_assignments': kids_assignments
	}
    return render(request, 'posts/index.html', context)


def post_details(request, id):
    post = Posts.objects.get(id=id)
    context = { 'post': post }
    return render(request, 'posts/post_details.html', context)


def assignment_details(request, id):
    assignment = Assignments.objects.get(id=id)
    context = { 'assignment': assignment }
    return render(request, 'posts/assignment_details.html', context)


def kids_assignment_details(request, id):
    kids_assignment = KidsAssignments.objects.get(id=id)
    context = { 'kids_assignment': kids_assignment }
    return render(request, 'posts/kids_assignment_details.html', context)


def load_experimental(request):
    posts = Posts.objects.all()[:10]
    assignments = Assignments.objects.all()[:10]
    kids_assignments = KidsAssignments.objects.all()[:10]
    context = {
    	'posts': posts,
	'assignments': assignments,
	'kids_assignments': kids_assignments
	}
    return render(request, 'posts/experimental.html', context)
