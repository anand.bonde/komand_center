from django.contrib import admin

from .models import Posts, Assignments, KidsAssignments

admin.site.register(Posts)
admin.site.register(Assignments)
admin.site.register(KidsAssignments)
