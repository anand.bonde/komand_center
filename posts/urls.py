from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('experimental/', views.load_experimental, name='experimental'),
    path('posts/post_details/<int:id>', views.post_details, name='post_details'),
    path('posts/assignment_details/<int:id>', views.assignment_details, name='assignment_details'),
    path('posts/kids_assignment_details/<int:id>', views.kids_assignment_details, name='kids_assignment_details')
]
